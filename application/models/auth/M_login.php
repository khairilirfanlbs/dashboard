<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_login extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function proses(){
        $user = trim($this->input->post('username','true'));
        $pass = trim(md5($this->input->post('password','true')));
        return $this->db->get_where('user',['username'=>$user,'password'=>$pass]);
    }
}
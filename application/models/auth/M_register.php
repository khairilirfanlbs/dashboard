<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_register extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function checkAccount(){
        $email      = trim($this->input->post('email','true'));
        $username   = trim($this->input->post('username','true'));
        return $this->db->where('email',$email)
                        ->or_where('username',$username)
                        ->get('user');
    }

    public function proses(){
        $ds = ldap_connect("ldap://ldap.unimed.ac.id");
        ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);
        $ldapbind = ldap_bind($ds, "cn=ldapbind,ou=SSO,dc=unimed,dc=ac, dc=id", "P@ssw0rd2020");
        $created_at = date('Y-m-d H:i:s');
        $updated_at = date('Y-m-d H:i:s');

        $nama_depan     = trim($this->input->post('nama_depan','true'));
        $nama_blakang = trim($this->input->post('nama_blakang','true'));
        $nama_full = $nama_depan . ' ' . $nama_blakang;
        $email    = trim($this->input->post('email','true'));    
        $username = trim($this->input->post('username','true'));
        $password = trim($this->input->post('password','true'));
        $no_hp  = trim($this->input->post('no_hp','true'));

        $this->db->trans_begin();
        // $this->db->insert('user',
        //                 [
        //                 'nama'          => $nama,
        //                 'email'         => $email,
        //                 'username'      => $username,
        //                 'password'      => md5($password),
        //                 'no_telp'       => $no_telp,
        //                 'id_role'       => '2',
        //                 'created_at'    => $created_at,
        //                 'updated_at'    => $updated_at,
        //                 ]
        //         );
        // $data = array(
        //     "cn" => $nama,
        //     "sn" => $nama
        //     "objectclass" => "User"
        // );
        $data['cn'] = $username;
        $data['sn'] = $nama_blakang;
        $data['uid'] = $username;
        $data['displayname'] = $nama_full;
        $data['givenname'] = $nama_depan;
        $data['mail'] = $email;
        $data["sAMAccountName"] = $username;
        $data['userPrincipalName'] = $email;
        $data['telephoneNumber'] = $no_hp;
        $data['objectclass'] = "User";
        ldap_add($ds, "cn = " . $username . ",ou = Users, ou = SSO, dc=unimed, dc=ac, dc=id", $data);
        ldap_close($ds);

        if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}else{
			$this->db->trans_commit();
			return TRUE;
		}
    }

}
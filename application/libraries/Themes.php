<?php
if(!defined('BASEPATH')) exit('no file allowed');
class Themes{
    protected $_ci;
     function __construct(){
        $this->_ci =&get_instance();
    }
    function Admin($theme, $data=null){
        $data['content']=$this->_ci->load->view($theme,$data,true);
        $data['header_bottom']=$this->_ci->load->view('theme/admin-section/header_bottom.php',$data,true);
        $data['header_top']=$this->_ci->load->view('theme/admin-section/header_top.php',$data,true);
        $data['menu']=$this->_ci->load->view('theme/admin-section/menu.php',$data,true);
        $data['footer']=$this->_ci->load->view('theme/admin-section/footer.php',$data,true);
        $data['modal']=$this->_ci->load->view('theme/admin-section/modal.php',$data,true);
        $data['js']=$this->_ci->load->view('theme/admin-section/js.php',$data,true);
        $this->_ci->load->view('theme/AdminTemplate.php', $data);
    }

    function Portal($themeWebOther, $data=null){
        $data['content']=$this->_ci->load->view($themeWebOther,$data,true);
        $data['header']=$this->_ci->load->view('theme/portal/header.php',$data,true);
        $data['footer']=$this->_ci->load->view('theme/portal/footer.php',$data,true);
        $data['banner']=$this->_ci->load->view('theme/portal/banner.php',$data,true);
        $data['js']=$this->_ci->load->view('theme/portal/js.php',$data,true);
        $data['css']=$this->_ci->load->view('theme/portal/css.php',$data,true);
        $this->_ci->load->view('theme/DefaultTemplate.php', $data);
    }

    function Auth($theme, $data=null){
        $data['_config_content']=$this->_ci->load->view($theme,$data,true);
        $data['js']=$this->_ci->load->view('theme/auth/js.php',$data,true);
        $this->_ci->load->view('theme/AuthTemplate.php', $data);
    }

}
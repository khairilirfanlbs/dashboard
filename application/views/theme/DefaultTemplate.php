<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="format-detection" content="telephone=no">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= APP_NAME ?> | <?= $title ?></title>


    <link rel="shortcut icon" href="<?= base_url() ?>assets/logo.png" />
    <link href="<?= base_url() ?>assets/portal/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/portal/css/animate.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/portal/css/fontawesome-all.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/portal/css/line-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link href="<?= base_url() ?>assets/portal/css/magnific-popup/magnific-popup.css" rel="stylesheet"
        type="text/css" />
    <link href="<?= base_url() ?>assets/portal/css/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/portal/css/base.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/portal/css/shortcodes.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/portal/css/style.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/portal/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/portal/css/theme-color/color-9.css" data-style="styles" rel="stylesheet">
    <link href="<?= base_url() ?>assets/portal/css/color-customize/color-customizer.css" rel="stylesheet"
        type="text/css" />
    <?= $css ?>
</head>

<body>
    <div class="page-wrapper">
        <!-- preloader start -->
        <div id="ht-preloader">
            <div class="clear-loader">
                <div class="loader">

                    <div class="loader-div">

                    </div>
                </div>
            </div>
        </div>
        <?= $header;?>
        <!-- preloader end -->
        <div class="page-content">
            <?= $banner;?>
            <?= $content;?>
        </div>
        <?= $footer;?>
    </div>
    <div class="scroll-top"><a class="smoothscroll" href="#top"><i class="la la-hand-pointer-o"></i></a></div>

    <script src="<?= base_url() ?>assets/portal/js/common-theme.js"></script>
    <script src="<?= base_url() ?>assets/portal/js/jquery.nice-select.js"></script>
    <script src="<?= base_url() ?>assets/portal/js/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?= base_url() ?>assets/portal/js/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url() ?>assets/portal/js/counter/counter.js"></script>
    <script src="<?= base_url() ?>assets/portal/js/isotope/isotope.pkgd.min.js"></script>
    <script src="<?= base_url() ?>assets/portal/js/particles.min.js"></script>
    <script src="<?= base_url() ?>assets/portal/js/vivus/pathformer.js"></script>
    <script src="<?= base_url() ?>assets/portal/js/vivus/vivus.js"></script>
    <script src="<?= base_url() ?>assets/portal/js/raindrops/jquery-ui.js"></script>
    <script src="<?= base_url() ?>assets/portal/js/raindrops/raindrops.js"></script>
    <script src="<?= base_url() ?>assets/portal/js/countdown/jquery.countdown.min.js"></script>
    <script src="<?= base_url() ?>assets/portal/js/contact-form/contact-form.js"></script>
    <script src="<?= base_url() ?>assets/portal/js/wow.min.js"></script>
    <script src="<?= base_url() ?>assets/portal/js/color-customize/color-customizer.js"></script>
    <script src="<?= base_url() ?>assets/portal/js/theme-script.js"></script>
    <?= $js ?>
</body>

</html>
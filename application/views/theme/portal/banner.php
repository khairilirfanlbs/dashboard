<section class="banner p-0 pos-r fullscreen-banner theme-bg bg-cover" data-bg-img="<?= base_url() ?>assets/portal/images/bg/03.png">
    <div class="align-center">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12">
                    <div class="image-anim">
                        <img class="img-center wow fadeInUp" data-wow-duration="2.5s" src="<?= base_url() ?>assets/portal/images/banner/05.png" alt="">
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 md-mt-5 wow fadeInDown" data-wow-duration="2.5s">
                    <h5 class="title-bg px-2 py-2 text-white">UNIVERSITAS NEGERI MEDAN</h5>
                    <h1 class="mb-4 font-w-7 text-white">SSO (Single Sign On)</h1>
                    <p class="lead mb-4 text-white">Memungkinkan pengguna jaringan agar dapat mengakses sumber daya dalam jaringan hanya dengan menggunakan satu akun pengguna saja
5. Registrasi</p> <a class="btn btn-white"
                        href="<?= base_url('registrasi') ?>"><span>Registrasi</span></a>
                    <a class="btn btn-border" href="<?= base_url('masuk') ?>"><span>Masuk</span></a>
                </div>
            </div>
        </div>
    </div>
</section>
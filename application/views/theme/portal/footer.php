<div id="waterdrop"></div>
<footer class="footer theme-bg">
    <div class="primary-footer">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 col-md-6">
                    <div class="footer-logo mb-3">
                        <img id="footer-logo-white-img" src="<?= base_url() ?>assets/logo.png" class="img-center"
                            alt="">
                    </div>
                    <p class="mb-0">Universitas Negeri Medan terus berkembang untuk meningkatkan kualitas dan akan terus
                        memberikan pelayanan terbaik di lingkungan universitas.</p>

                </div>
                <div class="col-lg-4 col-md-6 sm-mt-5">
                    <h4 class="mb-4 text-white">Link Terkait</h4>
                    <div class="footer-list justify-content-between d-flex">
                        <ul class="list-unstyled w-100">
                            <li><a href="https://unimed.ac.id">Portal UNIMED</a>
                            </li>
                            <li><a href="https://simremlink.unimed.ac.id">SIMREMLINK</a>
                            </li>
                            <li><a href="https://jdih.unimed.ac.id">JDIH</a>
                            </li>

                        </ul>
                        <ul class="list-unstyled w-100">
                            <li><a href="https://akad.unimed.ac.id">SIAKAD</a>
                            </li>
                            <li><a href="https://lppm.unimed.ac.id">LPPM</a>
                            </li>
                            <li><a href="https://simpeg.unimed.ac.id">SIMPEG</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 md-mt-5">
                    <div class="footer-cntct">
                        <h4 class="mb-4 text-white">UNIVERSITAS NEGERI MEDAN </h4>
                        <ul class="media-icon list-unstyled">
                            <li>
                                <p class="mb-0"><i class="la la-map-o"></i> <b>Jl. Willem Iskandar / Pasar V, Medan, Sumatera Utara – Indonesia
Kotak Pos 1589, Kode Pos 20221</b>
                                </p>
                            </li>
                            <li><i class="la la-envelope-o"></i> <a
                                    href="mailto:humas@unimed.ac.id"><b>humas@unimed.ac.id</b></a>
                            </li>
                            <li><i class="la la-phone"></i> <a href="tel:+626613365"><b>(061) 6613365, Fax. (061) 6614002 / 6613319</b></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="secondary-footer mt-5 text-center">
        <div class="container">
            <div class="copyright">
                <div class="row">
                    <div class="col-md-12"> <span><?= APP_COPYRIGHT ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
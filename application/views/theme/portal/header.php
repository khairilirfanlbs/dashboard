<header id="site-header" class="header header-2">
    <div id="header-wrap">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Navbar -->
                    <nav class="navbar navbar-expand-lg">
                        <a class="navbar-brand logo" href="index.html">
                            <img class="img-center" src="<?= base_url() ?>assets/logo.png" alt="">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav ml-auto mr-auto">
                                <!-- Home -->
                                <li class="nav-item"> <a class="nav-link active" href="#">Beranda</a></li>
                                <li class="nav-item"> <a class="nav-link" href="#">Aplikasi</a></li>
                                <li class="nav-item"> <a class="nav-link" href="#">Hubungi Kami</a></li>
                                <li class="nav-item"> <a class="nav-link" href="<?= base_url('registrasi') ?>">Registrasi</a></li>
                            </ul>
                        </div>
                        <div class="right-nav align-items-center d-flex justify-content-end list-inline">
                            <div class="header-call d-flex align-items-center mr-3">
                                <h5>Telp :</h5>
                                <a href="tel:+912345678900"><b>(061) 6613365</b></a>
                            </div> <a class="btn btn-white btn-sm" href="<?= base_url('masuk') ?>"><span>Masuk</span></a>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
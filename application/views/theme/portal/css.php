<style>
    .theme-bg,
    .btn.btn-theme,
    .nav-link.dropdown-toggle:hover::after,
    .header-2 #header-wrap.fixed-header,
    .owl-carousel .owl-dots .owl-dot:hover span,
    .owl-carousel .owl-dots .owl-dot.active span,
    .owl-carousel .owl-nav button.owl-prev:hover,
    .owl-carousel .owl-nav button.owl-next:hover,
    .tab .nav-tabs .nav-link.active,
    .tab .nav-tabs .nav-link:hover,
    .tab .nav-tabs.active>a:focus,
    .tab .nav-tabs>a:hover,
    .scroll-top a,
    .pagination>li.active .page-link,
    .page-link:hover,
    .page-link:focus,
    .video-btn .play-btn,
    .left-bdr,
    .right-bdr,
    .price-table.style-2:hover,
    .price-table.style-2.active,
    .price-table.style-3:hover,
    .price-table.style-3.active,
    .featured-item.style-4:hover,
    [data-overlay].theme-overlay::before,
    .widget .widget-tags li a:hover,
    .post-tags li a:hover,
    #sidebar .sidebar-header,
    #sidebar ul li.active>a,
    #sidebar a[aria-expanded="true"],
    #sidebar ul li:hover>a {
        background-color: #1da552;
    }

    .grediant-overlay::before,
    .grediant-bg,
    .featured-item.style-5 .featured-icon::before,
    .team-link a,
    .post.style-4 .post-image::before,
    .hero-shape1,
    .hero-shape2,
    .hero-shape4,
    .hero-shape5,
    .hero-shape6 {
        background: linear-gradient(58deg, rgb(29, 165, 81) 35%, rgb(239, 173, 0) 100%);
    }

    a,
    button,
    input,
    a:focus,
    a:hover,
    .text-theme,
    .nav-item.dropdown .dropdown-menu a:hover,
    .navbar-nav .nav-link:hover,
    .navbar-nav .nav-item .nav-link.active,
    h1 span,
    .breadcrumb-item.active,
    .breadcrumb-item a:hover,
    .featured-item .featured-icon i,
    .list-icon li i,
    .price-value h2,
    .price-list li i,
    .price-table.style-2 .price-title,
    .post-meta ul li i,
    .post .post-desc h4 a:hover,
    .media-icon li a:hover,
    .scroll-top a:hover,
    .scroll-top a:focus,
    .step-icon,
    .testimonial .testimonial-caption h5,
    .counter.style-2 span,
    .counter.style-3 span,
    .team-member .team-description h5,
    .job-list-icon i,
    .job-list-info i,
    .accordion .card.active a,
    .accordion .card a:hover,
    .contact-media h5,
    .contact-media i,
    .color-customizer a.opener,
    .widget-title,
    .widget-about h4,
    .widget .widget-categories a span,
    .comment-date,
    .comment-reply a:hover,
    .widget .widget-categories li a:hover,
    .widget .recent-post-desc a:hover,
    .widget-searchbox .search-btn {
        color: #1da552;
    }

    .featured-item.style-2::before,
    .featured-item.style-2::after {
        border-color: #efad00;
    }

    .bd-color-2,
    .bd-color-1  {
        stroke: #efad00;
    }
    
    </style>
<!--**********************************
            Sidebar start
        ***********************************-->
        <div class="deznav">
            <div class="deznav-scroll">
                <ul class="metismenu" id="menu">
                    <li class="nav-label first">Main Menu</li>
                    <li class="mega-menu mega-menu-lg">
                        <a class="ai-icon" href="<?= base_url('dashboard') ?>" aria-expanded="false">
                            <svg id="icon-dashboard" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-globe"><circle cx="12" cy="12" r="10"></circle><line x1="2" y1="12" x2="22" y2="12"></line><path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path></svg>
							<span class="nav-text">Dashboard</span>
						</a>
                    </li>

                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <svg id="icon-setting" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-server"><rect x="2" y="2" width="20" height="8" rx="2" ry="2"></rect><rect x="2" y="14" width="20" height="8" rx="2" ry="2"></rect><line x1="6" y1="6" x2="6" y2="6"></line><line x1="6" y1="18" x2="6" y2="18"></line></svg>
							<span class="nav-text">Setting</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a href="<?= base_url('setting/komponen-retribusi') ?>">Komponen Retribusi</a></li>
                            <li><a href="<?= base_url('setting/jabatan-petugas') ?>">Jabatan Petugas</a></li>
                        </ul>
                    </li>

                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <svg id="icon-user" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
							<span class="nav-text">User</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a href="<?= base_url('user/data-user') ?>">Data User</a></li>
                            <li><a href="<?= base_url('user/role-user') ?>">Role User</a></li>
                            <li><a href="<?= base_url('user/akses-user') ?>">Akses User</a></li>
                        </ul>
                    </li>

                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <svg id="icon-pedagang" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
							<span class="nav-text">Pedagang</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a href="javascript:void()" onclick="underConstruction();">Pedagang Baru</a></li>
                            <li><a href="javascript:void()" onclick="underConstruction();">Data Pedagang</a></li>
                        </ul>
                    </li>

                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <svg id="icon-master" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-grid"><rect x="3" y="3" width="7" height="7"></rect><rect x="14" y="3" width="7" height="7"></rect><rect x="14" y="14" width="7" height="7"></rect><rect x="3" y="14" width="7" height="7"></rect></svg>
							<span class="nav-text">Master</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a href="<?= base_url('master/jenis-tempat') ?>">Jenis Tempat</a></li>
                            <li><a href="<?= base_url('master/kelas-pasar') ?>">Kelas Pasar</a></li>
                            <li><a href="<?= base_url('master/bentuk-bangunan') ?>">Bentuk Bangunan</a></li>
                            <li><a href="<?= base_url('master/kondisi-bangunan') ?>">Kondisi Bangunan</a></li>
                            <li><a href="<?= base_url('master/surat-kepemilikan') ?>">Surat Kepemilikan</a></li>
                            <li><a href="<?= base_url('master/jenis-dagangan') ?>">Jenis Dagangan</a></li>
                            <li><a href="<?= base_url('master/jenis-pasar') ?>">Jenis Pasar</a></li>
                        </ul>
                    </li>

                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <svg id="icon-data-pasar" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
							<span class="nav-text">Pasar</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a href="<?= base_url('pasar/penanggung-jawab') ?>">Penanggung Jawab</a></li>
                            <li><a href="<?= base_url('pasar/data-pasar') ?>">Data Pasar</a></li>
                            <li><a href="<?= base_url('pasar/data-lantai') ?>">Data Lantai</a></li>
                            <li><a href="<?= base_url('pasar/data-blok') ?>">Data Blok</a></li>
                            <li><a href="<?= base_url('pasar/data-los') ?>">Data Los</a></li>
                            <li><a href="<?= base_url('pasar/data-kios') ?>">Data Kios</a></li>
                            <li><a href="<?= base_url('pasar/data-ruko') ?>">Data Ruko</a></li>
                        </ul>
                    </li>

                    <li class="mega-menu mega-menu-lg">
                        <a class="ai-icon" href="<?= base_url('petugas/data-petugas') ?>" aria-expanded="false">
                            <svg id="icon-data-petugas" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
							<span class="nav-text">Data Petugas</span>
						</a>
                    </li>

                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <svg id="icon-surat-pendasaran" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
							<span class="nav-text">Transaksi Pendasaran</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a href="javascript:void()" onclick="underConstruction();">Surat Pendasaran</a></li>
                            <li><a href="javascript:void()" onclick="underConstruction();">Balik Nama</a></li>
                        </ul>
                    </li>

                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <svg id="icon-skrd" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
							<span class="nav-text">Transaksi Retribusi</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a href="javascript:void()" onclick="underConstruction();">SKRD</a></li>
                            <li><a href="javascript:void()" onclick="underConstruction();">Pembayaran Retribusi</a></li>
                        </ul>
                    </li>

                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                    <svg id="icon-Pengajuan " xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign">
													<line x1="12" y1="1" x2="12" y2="23"></line>
													<path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path>
												</svg>
							<span class="nav-text">Transaksi Kontrak</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a href="javascript:void()" onclick="underConstruction();">Pengajuan Kontrak</a></li>
                            <li><a href="javascript:void()" onclick="underConstruction();">Perpanjang Kontrak</a></li>
                            <li><a href="javascript:void()" onclick="underConstruction();">Pembayaran Kontrak</a></li>
                            <li><a href="javascript:void()" onclick="underConstruction();">Pembatalan Kontrak</a></li>
                        </ul>
                    </li>

                    <li class="mega-menu mega-menu-lg">
                        <a class="ai-icon" href="#" aria-expanded="false" onclick="underConstruction();">
                            <svg id="icon-riwayat-pembayaran" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-airplay"><path d="M5 17H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-1"></path><polygon points="12 15 17 21 7 21 12 15"></polygon></svg>
							<span class="nav-text">Riwayat Pembayaran</span>
						</a>
                    </li>

                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <svg id="icon-laporan" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
							<span class="nav-text">Laporan</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a href="javascript:void()" onclick="underConstruction();">Pedagang Per Pasar</a></li>
                            <li><a href="javascript:void()" onclick="underConstruction();">Pendasaran</a></li>
                            <li><a href="javascript:void()" onclick="underConstruction();">Retribusi Pedagang</a></li>
                            <li><a href="javascript:void()" onclick="underConstruction();">Pembayaran Retribusi</a></li>
                        </ul>
                    </li>                    
                    
                </ul>
            </div>


        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->
<!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a href="<?= base_url('dashboard') ?>" class="brand-logo">
                <img class="logo-abbr" src="<?= base_url() ?>assets/logo-side.png" alt="">
                <img class="logo-compact" src="<?= base_url() ?>assets/logo-text.png" alt="">
                <img class="brand-title" src="<?= base_url() ?>assets/logo-text.png" alt="">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= APP_NAME ?> | <?= $title ?></title>
    <link rel="shortcut icon" href="<?= base_url() ?>assets/logo.png" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900|Montserrat:300,400,500,600,700,800,900" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css"
        href="<?= base_url() ?>assets/admin/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/vendors/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/vendors/css/prism.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN APEX CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/app.css">
    
</head>

<body data-col="1-column" class=" 1-column  blank-page">
    <?= $_config_content ?>
    <!-- BEGIN VENDOR JS-->
    <script src="<?= base_url() ?>assets/admin/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/admin/vendors/js/core/popper.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/admin/vendors/js/core/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/admin/vendors/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/admin/vendors/js/prism.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/admin/vendors/js/jquery.matchHeight-min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/admin/vendors/js/screenfull.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/admin/vendors/js/pace/pace.min.js" type="text/javascript"></script>
    
    <script src="<?= base_url() ?>assets/admin/js/app-sidebar.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/admin/js/notification-sidebar.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/admin/js/customizer.js" type="text/javascript"></script>
    <?= $js ?>
</body>

</html>
<section>
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-8 col-md-12">
                <div class="section-title">
                    <h2 class="title">Subscribe Our <span>Newsletter</span></h2>
                    <div class="title-bdr">
                        <div class="left-bdr"></div>
                        <div class="right-bdr"></div>
                    </div>
                    <p>Get started for 1 Month free trial No Purchace required.</p>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-lg-8 col-md-10 ml-auto mr-auto">
                <div class="subscribe-form">
                    <form id="mc-form" class="group d-md-flex align-items-center">
                        <input type="email" value="" name="EMAIL" class="email" id="mc-email"
                            placeholder="Enter Email Address" required="">
                        <input class="btn btn-theme" type="submit" name="subscribe" value="Subscribe">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
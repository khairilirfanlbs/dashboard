<section class="pos-r bg-contain bg-pos-rb" data-bg-img="<?= base_url() ?>assets/portal/images/bg/07.png">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="section-title">
                    <h2 class="title">Tentang Universitas Negeri Medan</h2>
                    <div class="title-bdr">
                        <div class="left-bdr"></div>
                        <div class="right-bdr"></div>
                    </div>
                </div>
                <p class="lead text-black">Universitas Negeri Medan berdiri pada 23 Juni 1963 dengan nama Institut Keguruan dan Ilmu Pendidikan (IKIP) Negeri Medan. </p>
                <p>Perubahan IKIP Medan menjadi Universitas dimaksudkan sebagai upaya peningkatan mutu penyelenggaraan Lembaga Pendidikan Tenaga Kependidikan (LPTK). Perubahan ini pada gilirannya ditempatkan sebagai upaya untuk meningkatkan mutu lulusan yang dipandang relevan untuk menjawab kebutuhan pembangunan di berbagai bidang. </p>
                <a class="btn btn-white" href="https://www.unimed.ac.id/sejarah-unimed/"><span>Selengkapnya</span></a>
            </div>
            <div class="col-lg-6 col-md-12 md-mt-5">
                <div class="row">
                    <div class="col-lg-6 col-md-6 text-right">
                        <div class="counter style-2">
                            <div class="counter-desc"> <span class="count-number" data-to="234"
                                    data-speed="10000">234</span>
                                <span>+</span>
                                <h5>Mahasiswa</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 sm-mt-5 text-right">
                        <div class="counter style-2">
                            <div class="counter-desc"> <span class="count-number" data-to="455"
                                    data-speed="10000">455</span>
                                <span>+</span>
                                <h5>Dosen</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 md-mt-5">
                        <div class="counter style-2">
                            <div class="counter-desc"> <span class="count-number" data-to="365"
                                    data-speed="10000">365</span>
                                <span>+</span>
                                <h5>Tendik</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 md-mt-5">
                        <div class="counter style-2">
                            <div class="counter-desc"> <span class="count-number" data-to="528"
                                    data-speed="10000">528</span>
                                <span>+</span>
                                <h5>Luas Wilayah</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--text block start-->

<section class="pos-r bg-contain bg-pos-lb" data-bg-img="<?= base_url() ?>assets/portal/images/bg/06.png">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-12 image-column">
                <img class="img-center" src="<?= base_url() ?>assets/portal/images/about/02.svg" alt="">
            </div>
            <div class="col-lg-6 col-md-12 ml-auto md-mt-5">
                <div class="section-title">
                    <h2 class="title">Integrasi Aplikasi</h2>
                    <div class="title-bdr">
                        <div class="left-bdr"></div>
                        <div class="right-bdr"></div>
                    </div>
                    <p class="mb-0 text-black"> Semua aplikasi terintegrasi dalam satu portal utama.</p>
                </div>
                <div class="row mb-5">
                    <div class="col-md-6">
                        <ul class="list-unstyled list-icon">
                            <li class="mb-3"><i class="fas fa-check-circle"></i> Kepegawaian</li>
                            <li class="mb-3"><i class="fas fa-check-circle"></i> Akademik</li>
                            <li><i class="fas fa-check-circle"></i> Penelitian</li>
                        </ul>
                    </div>
                    <div class="col-md-6 sm-mt-2">
                        <ul class="list-unstyled list-icon">
                            <li class="mb-3"><i class="fas fa-check-circle"></i> Pengabdian</li>
                            <li class="mb-3"><i class="fas fa-check-circle"></i> JDHI</li>
                            <li><i class="fas fa-check-circle"></i> Remunerasi</li>
                        </ul>
                    </div>
                </div> <a class="btn btn-border btn-circle" href="#"><span>Lihat Selengkapnya</span>
                </a>
            </div>
        </div>
    </div>
</section>

<!--text block end-->
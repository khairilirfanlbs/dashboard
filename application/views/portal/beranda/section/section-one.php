<section class="grey-bg py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="owl-carousel no-pb" data-dots="false" data-items="5" data-md-items="4" data-sm-items="3"
                    data-margin="30" data-autoplay="true">
                    <div class="item">
                        <div class="client-logo style-2">
                            <img class="img-center" src="<?= base_url() ?>assets/portal/images/client/bni.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="client-logo style-2">
                            <img class="img-center" src="<?= base_url() ?>assets/portal/images/client/02.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="client-logo style-2">
                            <img class="img-center" src="<?= base_url() ?>assets/portal/images/client/03.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="client-logo style-2">
                            <img class="img-center" src="<?= base_url() ?>assets/portal/images/client/04.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="client-logo style-2">
                            <img class="img-center" src="<?= base_url() ?>assets/portal/images/client/05.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="client-logo style-2">
                            <img class="img-center" src="<?= base_url() ?>assets/portal/images/client/06.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="client-logo style-2">
                            <img class="img-center" src="<?= base_url() ?>assets/portal/images/client/07.png" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="client-logo style-2">
                            <img class="img-center" src="<?= base_url() ?>assets/portal/images/client/08.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
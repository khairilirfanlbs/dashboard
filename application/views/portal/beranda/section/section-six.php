<section>
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-8 col-md-10 ml-auto mr-auto">
                <div class="section-title">
                    <h2 class="title">Aplikasi Terintegrasi</h2>
                    <div class="title-bdr">
                        <div class="left-bdr"></div>
                        <div class="right-bdr"></div>
                    </div>
                    <p>Aplikasi dibawah ini sudah terintegrasi satu dengan yang lainnya sehingga hanya butuh satu akun saja untuk mengakses semua layanan aplikasi.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="featured-item style-5">
                    <div class="featured-icon"> <i class="las la-user-tie" style="font-size: 50px;"></i>
                    </div>
                    <div class="featured-title">
                        <h5>SIMPEG</h5>
                    </div>
                    <div class="featured-desc">
                        <p>Top quality Software services, labore et dolore magna ali qua Lorem ipsum dolor sit
                            amet.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 sm-mt-5">
                <div class="featured-item style-5">
                    <div class="featured-icon"> <i class="las la-user-graduate" style="font-size: 50px;"></i>
                    </div>
                    <div class="featured-title">
                        <h5>SIAKAD</h5>
                    </div>
                    <div class="featured-desc">
                        <p>Top quality Software services, labore et dolore magna ali qua Lorem ipsum dolor sit
                            amet.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 md-mt-5">
                <div class="featured-item style-5">
                    <div class="featured-icon"> <i class="las la-mail-bulk" style="font-size: 50px;"></i>
                    </div>
                    <div class="featured-title">
                        <h5>JDHI</h5>
                    </div>
                    <div class="featured-desc">
                        <p>Top quality Software services, labore et dolore magna ali qua Lorem ipsum dolor sit
                            amet.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 mt-5">
                <div class="featured-item style-5">
                    <div class="featured-icon"> <i class="las la-flask" style="font-size: 50px;"></i>
                    </div>
                    <div class="featured-title">
                        <h5>LPPM</h5>
                    </div>
                    <div class="featured-desc">
                        <p>Top quality Software services, labore et dolore magna ali qua Lorem ipsum dolor sit
                            amet.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 mt-5">
                <div class="featured-item style-5">
                    <div class="featured-icon"> <i class="las la-award" style="font-size: 50px;"></i>
                    </div>
                    <div class="featured-title">
                        <h5>SIMREMLINK</h5>
                    </div>
                    <div class="featured-desc">
                        <p>Top quality Software services, labore et dolore magna ali qua Lorem ipsum dolor sit
                            amet.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- $2y$10$FUKBCuRRor1xtSniufQ4OevrGu9zhpTIIBiOohLip4zUORUU1FDny -->
<!--services end-->
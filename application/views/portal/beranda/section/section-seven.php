<!--testimonial start-->

<section class="dark-bg custom-pb-1 pos-r">
    <div class="wave-box">
        <div class="wave"></div>
        <div class="wave"></div>
    </div>
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-8 col-md-12">
                <div class="section-title mb-0">
                    <h2 class="title">Discover Our <span>Tranding Template</span></h2>
                    <div class="title-bdr">
                        <div class="left-bdr"></div>
                        <div class="right-bdr"></div>
                    </div>
                    <p class="text-white">Powerfull Template customer service with our tools, hendrerit
                        omittantur mel, es vidit Ius te omittantur meles vidit Ius te altera essent incorrupte.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="p-0 custom-mt-10 custom-tab">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="tab">
                    <!-- Nav tabs -->
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-tab1" data-toggle="tab" href="#tab1-1"
                                role="tab" aria-selected="true">
                                Digital Marketing
                            </a> <a class="nav-item nav-link" id="nav-tab2" data-toggle="tab" href="#tab1-2" role="tab"
                                aria-selected="false">
                                Design</a>
                            <a class="nav-item nav-link" id="nav-tab3" data-toggle="tab" href="#tab1-3" role="tab"
                                aria-selected="false">
                                Email Marketing</a>
                            <a class="nav-item nav-link" id="nav-tab4" data-toggle="tab" href="#tab1-4" role="tab"
                                aria-selected="false">
                                Data Anlysis</a>
                        </div>
                    </nav>
                    <!-- Tab panes -->
                    <div class="tab-content" id="nav-tabContent">
                        <div role="tabpanel" class="tab-pane fade show active" id="tab1-1">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-md-12">
                                    <img class="img-fluid" src="<?= base_url() ?>assets/portal/images/svg/01.svg" alt="">
                                </div>
                                <div class="col-lg-6 col-md-12 md-mt-5">
                                    <h3 class="mb-4">Sassaht <span class="font-w-7 text-theme">Powerful &
                                            Awesome</span> Template For Marketing</h3>
                                    <p class="mb-4">Sassaht Ut enim ad minim veniam, quis nostrud exercitation
                                        ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    <ul class="list-icon custom-li list-unstyled d-inline-block">
                                        <li><i class="fas fa-check-circle"></i> Design must be functional</li>
                                        <li><i class="fas fa-check-circle"></i> Futionality must into</li>
                                        <li><i class="fas fa-check-circle"></i> Aenean pellentes vitae</li>
                                        <li><i class="fas fa-check-circle"></i> Mattis effic iturut magna</li>
                                        <li><i class="fas fa-check-circle"></i> Lusce enim nulla mollis</li>
                                        <li><i class="fas fa-check-circle"></i> Phasellus eget felis</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab1-2">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-md-12">
                                    <img class="img-fluid" src="<?= base_url() ?>assets/portal/images/svg/02.svg" alt="">
                                </div>
                                <div class="col-lg-6 col-md-12 md-mt-5">
                                    <h3 class="mb-4">Sassaht Provide <span class="font-w-7 text-theme">Creative
                                            & Modern</span> Design</h3>
                                    <p class="mb-4">Sassaht Ut enim ad minim veniam, quis nostrud exercitation
                                        ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    <ul class="list-icon custom-li list-unstyled d-inline-block">
                                        <li><i class="fas fa-check-circle"></i> Design must be functional</li>
                                        <li><i class="fas fa-check-circle"></i> Futionality must into</li>
                                        <li><i class="fas fa-check-circle"></i> Aenean pellentes vitae</li>
                                        <li><i class="fas fa-check-circle"></i> Mattis effic iturut magna</li>
                                        <li><i class="fas fa-check-circle"></i> Lusce enim nulla mollis</li>
                                        <li><i class="fas fa-check-circle"></i> Phasellus eget felis</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab1-3">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-md-12">
                                    <img class="img-fluid" src="<?= base_url() ?>assets/portal/images/svg/03.svg" alt="">
                                </div>
                                <div class="col-lg-6 col-md-12 md-mt-5">
                                    <h3 class="mb-4">Sassaht With <span class="font-w-7 text-theme">Best
                                            Unique</span> Email Marketing Template</h3>
                                    <p class="mb-4">Sassaht Ut enim ad minim veniam, quis nostrud exercitation
                                        ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    <ul class="list-icon custom-li list-unstyled d-inline-block">
                                        <li><i class="fas fa-check-circle"></i> Design must be functional</li>
                                        <li><i class="fas fa-check-circle"></i> Futionality must into</li>
                                        <li><i class="fas fa-check-circle"></i> Aenean pellentes vitae</li>
                                        <li><i class="fas fa-check-circle"></i> Mattis effic iturut magna</li>
                                        <li><i class="fas fa-check-circle"></i> Lusce enim nulla mollis</li>
                                        <li><i class="fas fa-check-circle"></i> Phasellus eget felis</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab1-4">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-md-12">
                                    <img class="img-fluid" src="<?= base_url() ?>assets/portal/images/svg/04.svg" alt="">
                                </div>
                                <div class="col-lg-6 col-md-12 md-mt-5">
                                    <h3 class="mb-4">Sassaht <span class="font-w-7 text-theme">Powerful &
                                            Awesome</span> Template For Data Anlysis</h3>
                                    <p class="mb-4">Sassaht Ut enim ad minim veniam, quis nostrud exercitation
                                        ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    <ul class="list-icon custom-li list-unstyled d-inline-block">
                                        <li><i class="fas fa-check-circle"></i> Design must be functional</li>
                                        <li><i class="fas fa-check-circle"></i> Futionality must into</li>
                                        <li><i class="fas fa-check-circle"></i> Aenean pellentes vitae</li>
                                        <li><i class="fas fa-check-circle"></i> Mattis effic iturut magna</li>
                                        <li><i class="fas fa-check-circle"></i> Lusce enim nulla mollis</li>
                                        <li><i class="fas fa-check-circle"></i> Phasellus eget felis</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--testimonial end-->
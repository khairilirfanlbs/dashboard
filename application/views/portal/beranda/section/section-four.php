<section class="text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <center>
                    <div class="col-lg-3 col-md-6">
                        <div class="team-member style-1 active">
                            <div class="team-images">
                                <img class="img-fluid radius box-shadow"
                                    src="<?= base_url() ?>assets/portal/images/team/01.png" alt="">
                                <div class="social-icons social-colored circle team-social-icon">
                                    <ul>
                                        <li class="social-facebook"><a href="#"><i class="fab fa-facebook-f"></i></a>
                                        </li>
                                        <li class="social-twitter"><a href="#"><i class="fab fa-twitter"></i></a>
                                        </li>
                                        <li class="social-gplus"><a href="#"><i class="fab fa-google-plus-g"></i></a>
                                        </li>
                                        <li class="social-linkedin"><a href="#"><i class="fab fa-linkedin-in"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="team-description">
                                <h5>Dr. Syamsul Gultom. SKM. M.Kes</h5> <span>Rektor Unimed</span>
                                <div class="team-link"> <a href="#"><i class="la la-plus-circle"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </center>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="team-member style-1">
                    <div class="team-images">
                        <img class="img-fluid radius box-shadow" src="<?= base_url() ?>assets/portal/images/team/02.png"
                            alt="">
                        <div class="social-icons social-colored circle team-social-icon">
                            <ul>
                                <li class="social-facebook"><a href="#"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li class="social-twitter"><a href="#"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li class="social-gplus"><a href="#"><i class="fab fa-google-plus-g"></i></a>
                                </li>
                                <li class="social-linkedin"><a href="#"><i class="fab fa-linkedin-in"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-description">
                        <h5>Dr. Restu. M.S.</h5> <span>Wakil Rektor I </span>
                        <div class="team-link"> <a href="#"><i class="la la-plus-circle"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 sm-mt-5">
                <div class="team-member style-1">
                    <div class="team-images">
                        <img class="img-fluid radius box-shadow" src="<?= base_url() ?>assets/portal/images/team/03.png"
                            alt="">
                        <div class="social-icons social-colored circle team-social-icon">
                            <ul>
                                <li class="social-facebook"><a href="#"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li class="social-twitter"><a href="#"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li class="social-gplus"><a href="#"><i class="fab fa-google-plus-g"></i></a>
                                </li>
                                <li class="social-linkedin"><a href="#"><i class="fab fa-linkedin-in"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-description">
                        <h5>Dr. Martina Restuati. M.Si.</h5> <span>Wakil Rektor II</span>
                        <div class="team-link"> <a href="#"><i class="la la-plus-circle"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 md-mt-5">
                <div class="team-member style-1">
                    <div class="team-images">
                        <img class="img-fluid radius box-shadow" src="<?= base_url() ?>assets/portal/images/team/04.png"
                            alt="">
                        <div class="social-icons social-colored circle team-social-icon">
                            <ul>
                                <li class="social-facebook"><a href="#"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li class="social-twitter"><a href="#"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li class="social-gplus"><a href="#"><i class="fab fa-google-plus-g"></i></a>
                                </li>
                                <li class="social-linkedin"><a href="#"><i class="fab fa-linkedin-in"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-description">
                        <h5>Prof. Dr. Sahat Siagian. M.Pd.</h5> <span>Wakil Rektor III</span>
                        <div class="team-link"> <a href="#"><i class="la la-plus-circle"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 md-mt-5">
                <div class="team-member style-1">
                    <div class="team-images">
                        <img class="img-fluid radius box-shadow" src="<?= base_url() ?>assets/portal/images/team/05.png"
                            alt="">
                        <div class="social-icons social-colored circle team-social-icon">
                            <ul>
                                <li class="social-facebook"><a href="#"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li class="social-twitter"><a href="#"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li class="social-gplus"><a href="#"><i class="fab fa-google-plus-g"></i></a>
                                </li>
                                <li class="social-linkedin"><a href="#"><i class="fab fa-linkedin-in"></i></a>
                                </li>
                            </ul>
                        </div> <a class="team-link" href="#"><i class="la la-external-link"></i></a>
                    </div>
                    <div class="team-description">
                        <h5>Prof. Manihar Situmorang M.Sc.. Ph.D.</h5>
                        <span>Wakil Rektor IV</span>
                        <div class="team-link"> <a href="#"><i class="la la-plus-circle"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
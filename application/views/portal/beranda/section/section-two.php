<style>
.featured-item .featured-icon i {
    font-size: 100px;
}
.featured-item.style-2{
    height:490px;
}
.featured-desc p {
    height: 200px;
}
.featured-title h5 {
    height: 80px;
}
</style>
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="owl-carousel" data-dots="false" data-nav="true" data-items="3" data-md-items="2"
                    data-sm-items="2" data-autoplay="true">
                    <div class="item">
                        <div class="featured-item style-2">
                            <div class="featured-icon">
                                <i class="las la-chalkboard-teacher"></i>
                            </div>
                            <div class="featured-title">
                                <h5>Fakultas Ilmu Pendidikan</h5>
                            </div>
                            <div class="featured-desc">
                                <p>Pada Tahun 2025 Menjadi Fakultas Yang Unggul Dan Inovatif Dalam Bidang Ilmu Pendidikan Di Tingkat Nasional Menuju Internasiona</p> <a class="btn-iconic mt-1" href="http://fip.unimed.ac.id"><i
                                        class="la la-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="featured-item style-2">
                            <div class="featured-icon">
                                <i class="las la-language"></i>
                            </div>
                            <div class="featured-title">
                                <h5>Fakultas Bahasa dan Seni</h5>
                            </div>
                            <div class="featured-desc">
                                <p>Pada tahun 2025 menjadi fakultas yang unggul dalam pendidikan, pengkajian, dan rekayasa industri bidang bahasa, sastra, seni dan budaya, serta mendapat pengakuan pada tingkat nasional dan internasional.</p> <a class="btn-iconic mt-4" href="https://fbs.unimed.ac.id/"><i
                                        class="la la-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="featured-item style-2">
                            <div class="featured-icon">
                                <i class="las la-users"></i>
                            </div>
                            <div class="featured-title">
                                <h5>Fakultas Ilmu Sosial</h5>
                            </div>
                            <div class="featured-desc">
                                <p>Menjadi Fakultas yang unggul dalam pengembangan pendidikan dan ilmu-ilmu sosial</p> <a class="btn-iconic mt-4" href="https://fis.unimed.ac.id"><i
                                        class="la la-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="featured-item style-2">
                            <div class="featured-icon">
                                <i class="las la-atom"></i>
                            </div>
                            <div class="featured-title">
                                <h5>Fakultas Matematika Ilmu pengetahuan Alam</h5>
                            </div>
                            <div class="featured-desc">
                                <p>Menjadi Fakultas yang unggul dalam bidang pendidikan, penelitian, dan inovasi bidang MIPA berstandar nasional dan internasional.</p> <a class="btn-iconic mt-4" href="https://fmipa.unimed.ac.id/"><i
                                        class="la la-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="featured-item style-2">
                            <div class="featured-icon">
                                <i class="las la-laptop-code"></i>
                            </div>
                            <div class="featured-title">
                                <h5>Fakultas Teknik</h5>
                            </div>
                            <div class="featured-desc">
                                <p>Menjadi Fakultas yang unggul di bidang pendidikan kejuruan dan ilmu teknik, rekayasa industri dan budaya.</p> <a class="btn-iconic mt-4" href="https://ft.unimed.ac.id/"><i
                                        class="la la-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="featured-item style-2">
                            <div class="featured-icon">
                                <i class="las la-futbol"></i>
                            </div>
                            <div class="featured-title">
                                <h5>Fakultas Ilmu Keolahragaan</h5>
                            </div>
                            <div class="featured-desc">
                                <p>Fakultas yang memiliki tiga program studi, yaitu: Pendidikan Jasmani Kesehatan dan Rekreasi, Pendidikan Kepelatihan Olahraga dan Ilmu Keolahragaan.</p> <a class="btn-iconic mt-4" href="https://fik.unimed.ac.id"><i
                                        class="la la-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="featured-item style-2">
                            <div class="featured-icon">
                                <i class="las la-comment-dollar"></i>
                            </div>
                            <div class="featured-title">
                                <h5>Fakultas Ekonomi</h5>
                            </div>
                            <div class="featured-desc">
                                <p>Unggul dalam bidang pendidikan, Ilmu ekonomi dan Bisnis</p> <a class="btn-iconic mt-4" href="https://fe.unimed.ac.id/"><i
                                        class="la la-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="featured-item style-2">
                            <div class="featured-icon">
                                <i class="las la-user-graduate"></i>
                            </div>
                            <div class="featured-title">
                                <h5>Program Pascasarjana</h5>
                            </div>
                            <div class="featured-desc">
                                <p>Program Pascasarjana yang unggul dalam pembelajaran dan penelitian pendidikan, humaniora, sains dan teknologi yang diakui di tingkat nasional, regional dan internasional</p> <a class="btn-iconic mt-4" href="https://pps.unimed.ac.id/"><i
                                        class="la la-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
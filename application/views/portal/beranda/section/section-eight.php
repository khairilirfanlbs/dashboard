<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="testimonial-1" class="testimonial-carousel testimonial-1 carousel slide" data-ride="carousel"
                    data-interval="2500">
                    <!-- Wrapper for slides -->
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-md-12">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div class="testimonial style-1">
                                        <div class="testimonial-img">
                                            <img class="img-center" src="<?= base_url() ?>assets/portal/images/testimonial/01.jpg" alt="">
                                        </div>
                                        <div class="testimonial-content">
                                            <div class="testimonial-quote"><i class="fas fa-quote-left"></i>
                                            </div>
                                            <p>On the other hand, we denounce with righteous indignation and
                                                dislike men who are so beguiled and demoralized by the charms of
                                                pleasure of the moment looked up one of the more obscure Latin
                                                words</p>
                                            <div class="testimonial-caption">
                                                <h5>Jasse Lynn</h5>
                                                <label>Founder of Sassaht</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="testimonial style-1">
                                        <div class="testimonial-img">
                                            <img class="img-center" src="<?= base_url() ?>assets/portal/images/testimonial/02.jpg" alt="">
                                        </div>
                                        <div class="testimonial-content">
                                            <div class="testimonial-quote"><i class="fas fa-quote-left"></i>
                                            </div>
                                            <p>Temporibus autem quibusdam et aut officiis debitis aut rerum
                                                necessitatibus saepe eveniet ut et voluptates repudiandae sint
                                                et molestiae non recusandae predefined chunks as necessary,
                                                making</p>
                                            <div class="testimonial-caption">
                                                <h5>Thomas James</h5>
                                                <label>CEO of Sassaht</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="testimonial style-1">
                                        <div class="testimonial-img">
                                            <img class="img-center" src="<?= base_url() ?>assets/portal/images/testimonial/03.jpg" alt="">
                                        </div>
                                        <div class="testimonial-content">
                                            <div class="testimonial-quote"><i class="fas fa-quote-left"></i>
                                            </div>
                                            <p>Injected humour, or randomised words which don't look even
                                                slightly believable generators on the Internet tend to repeat
                                                predefined chunks as necessary generate Lorem Ipsum which looks
                                                reasonable</p>
                                            <div class="testimonial-caption">
                                                <h5>Ceathy White</h5>
                                                <label>Founder of Sassaht</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Item -->
                            </div>
                            <!-- End Carousel Inner -->
                        </div>
                    </div>
                    <div class="controls">
                        <ul class="nav">
                            <li data-target="#testimonial-1" data-slide-to="0" class="active">
                                <a href="#">
                                    <img class="img-fluid" src="<?= base_url() ?>assets/portal/images/testimonial/01.jpg" alt="">
                                </a>
                            </li>
                            <li data-target="#testimonial-1" data-slide-to="1">
                                <a href="#">
                                    <img class="img-fluid" src="<?= base_url() ?>assets/portal/images/testimonial/02.jpg" alt="">
                                </a>
                            </li>
                            <li data-target="#testimonial-1" data-slide-to="2">
                                <a href="#">
                                    <img class="img-fluid" src="<?= base_url() ?>assets/portal/images/testimonial/03.jpg" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- End Carousel -->
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-8 col-md-12">
                <div class="section-title">
                    <h2 class="title">See Our Latest <span>News Feed</span></h2>
                    <div class="title-bdr">
                        <div class="left-bdr"></div>
                        <div class="right-bdr"></div>
                    </div>
                    <p>Powerfull Template customer service with our tools, hendrerit omittantur mel, es vidit
                        Ius te omittantur meles vidit Ius te altera essent incorrupte.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-12">
                <div class="post">
                    <div class="post-image">
                        <img class="img-fluid h-100 w-100" src="<?= base_url() ?>assets/portal/images/blog/01.jpg" alt="">
                    </div>
                    <div class="post-desc">
                        <div class="post-meta">
                            <ul class="list-inline">
                                <li><i class="la la-calendar mr-1"></i> 23 Jan, 2019</li>
                                <li><i class="la la-user mr-1"></i> By ThemeHt</li>
                            </ul>
                        </div>
                        <div class="post-title">
                            <h4><a href="blog-single.html">Sassaht Built With Powerfull Sass</a></h4>
                        </div>
                        <div class="post-categories mt-4">
                            <ul class="list-inline">
                                <li>Sass</li>
                                <li>Marketing</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 md-mt-5">
                <div class="post">
                    <div class="post-image">
                        <img class="img-fluid h-100 w-100" src="<?= base_url() ?>assets/portal/images/blog/02.jpg" alt="">
                    </div>
                    <div class="post-desc">
                        <div class="post-meta">
                            <ul class="list-inline">
                                <li><i class="la la-calendar mr-1"></i> 23 Jan, 2019</li>
                                <li><i class="la la-user mr-1"></i> By ThemeHt</li>
                            </ul>
                        </div>
                        <div class="post-title">
                            <h4><a href="blog-single.html">latest Modern design in features</a></h4>
                        </div>
                        <div class="post-categories mt-4">
                            <ul class="list-inline">
                                <li>Sass</li>
                                <li>Marketing</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 md-mt-5">
                <div class="post">
                    <div class="post-image">
                        <img class="img-fluid h-100 w-100" src="<?= base_url() ?>assets/portal/images/blog/03.jpg" alt="">
                    </div>
                    <div class="post-desc">
                        <div class="post-meta">
                            <ul class="list-inline">
                                <li><i class="la la-calendar mr-1"></i> 23 Jan, 2019</li>
                                <li><i class="la la-user mr-1"></i> By ThemeHt</li>
                            </ul>
                        </div>
                        <div class="post-title">
                            <h4><a href="blog-single.html">All Powerful Design feature In Sassaht</a></h4>
                        </div>
                        <div class="post-categories mt-4">
                            <ul class="list-inline">
                                <li>Sass</li>
                                <li>Marketing</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 <!-- ////////////////////////////////////////////////////////////////////////////-->
 <div class="wrapper">
     <div class="main-panel">
         <!-- BEGIN : Main Content-->
         <div class="main-content">
             <div class="content-wrapper">
                 <!--Registration Page Starts-->
                 <section id="regestration">
                     <div class="container-fluid">
                         <div class="row full-height-vh m-0">
                             <div class="col-12 d-flex align-items-center justify-content-center">
                                 <div class="card">
                                     <div class="card-content">
                                         <div class="card-body register-img">
                                             <div class="row m-0">
                                                 <div class="col-lg-6 d-none d-lg-block py-2 text-center">
                                                     <img src="<?= base_url() ?>assets/admin/img/gallery/register.png" alt=""
                                                         class="img-fluid mt-3 pl-3" width="400" height="230">
                                                 </div>
                                                 <div class="col-lg-6 col-md-12 px-4 pt-3 bg-white">


                                                     <h4 class="card-title mb-2"><?= $title ?></h4>
                                                     <p class="card-text mb-3">
                                                         Isi form dibawah untuk membuat akun baru.
                                                     </p>
                                                
                                                     <form method="post" id="form-register">
                                                        <div class="form-row">
                                                            <div class="form-group col-md-6">
                                      
                                      <input type="text" class="form-control" placeholder="Name Depan" name="nama_depan" required />
                                    </div>
                                    <div class="form-group col-md-6">
                                      
                                      <input type="text" class="form-control" placeholder="Name Belakang" name="nama_blakang" required />
                                    </div>
                                                        </div>
                                                        
                                                        
                                                        <input type="text" class="form-control mb-3" placeholder="Username" name="username" id="username" required />

                                                        <input type="email" class="form-control mb-3" placeholder="Email" name="email" id="email" readonly />

                                                        <input type="text" class="form-control mb-3" placeholder="Nomor HP (WhatsApp)" name="no_hp" required />

                                                        <input type="password" class="form-control mb-3" placeholder="Password" name="password" id="password" required />

                                                        <input type="password" class="form-control" placeholder="Confirm Password" id="confirm_password" required />
                                                        <p id="info"></p>

                                                        <div class="custom-control custom-checkbox custom-control-inline mb-3">
                                                            <input type="checkbox" id="customCheckboxInline1"
                                                                name="customCheckboxInline1" class="custom-control-input"
                                                                checked />
                                                            <label class="custom-control-label"
                                                                for="customCheckboxInline1">
                                                                I accept the terms & conditions.
                                                            </label>
                                                        </div>
                                                        <div class="fg-actions d-flex justify-content-between">
                                                         <div class="login-btn">
                                                             <button class="btn btn-outline-success">
                                                                 <a href="#" class="text-decoration-none">
                                                                     Back To Login
                                                                 </a>
                                                             </button>
                                                         </div>
                                                         <div class="recover-pass">

                                                             <button class="btn btn-success" type="submit" id="btn_regis">
                                                                 Register
                                                             </button>
                                                         </div>
                                                        </div>
                                                     </form>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </section>
                 <!--Registration Page Ends-->

             </div>
         </div>
         <!-- END : End Main Content-->
     </div>
 </div>
 <!-- ////////////////////////////////////////////////////////////////////////////-->
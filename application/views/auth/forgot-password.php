<div class="form-body">
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">
                    <h3><?= APP_NAME ?></h3>
                    <p><?= APP_SUGGESTION ?></p>
                    <img src="<?= base_url(); ?>assets/auth/images/graphic5.svg" alt="">
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <div class="website-logo-inside">
                            <a href="#">
                                <img class="logo-size" src="<?= base_url(); ?>assets/logo.png" alt="">
                            </a>
                        </div>
                        <div class="page-links">
                            <a href="#" class="active">Forgot Password</a>
                        </div>
                        <div class="alert alert-primary bg-primary">
                                Masukkan E-Mail yang telah anda daftarkan.
                        </div>
                        <form method="post" id="form-data">
                            <input class="form-control" type="email" name="email" id="email" placeholder="Email ( Cth : example@email.com )" required>
                            <div class="form-button">
                                <button type="submit" class="btn btn-primary">Next Step </button> <a href="<?= base_url('/') ?>">Kembali login ?</a>
                            </div>
                        </form>
                        <div class="other-links">
                            <span><?= APP_COPYRIGHT ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
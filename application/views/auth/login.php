<div class="form-body">
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">
                    <h3><?= APP_NAME ?></h3>
                    <p><?= APP_SUGGESTION ?></p>
                    <img src="<?= base_url(); ?>assets/auth/images/graphic5.svg" alt="">
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <div class="website-logo-inside">
                            <a href="#">
                                <img class="logo-size" src="<?= base_url(); ?>assets/logo.png" alt="">
                            </a>
                        </div>
                        <div class="page-links">
                            <a href="#" class="active">Login</a>
                            <a href="<?= base_url('register') ?>" >Registrasi Pedagang Baru</a>
                        </div>
                        <form method="post" id="form-data">
                            <input class="form-control" type="text" name="username" id="userName" placeholder="Username" required>
                            <input class="form-control" type="password" name="password" id="password" placeholder="Password" required>
                            <div class="form-button">
                                <button id="submit" type="submit" class="btn btn-primary">Login</button> <a href="<?= base_url('forgot-password') ?>">Forgot password?</a>
                            </div>
                        </form>
                        <div class="other-links">
                            <span><?= APP_COPYRIGHT ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
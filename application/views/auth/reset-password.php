<div class="form-body">
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">
                    <h3><?= APP_NAME ?></h3>
                    <p><?= APP_SUGGESTION ?></p>
                    <img src="<?= base_url(); ?>assets/auth/images/graphic5.svg" alt="">
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <div class="website-logo-inside">
                            <a href="#">
                                <img class="logo-size" src="<?= base_url(); ?>assets/logo.png" alt="">
                            </a>
                        </div>
                        <div class="page-links">
                            <a href="#" class="active">Reset Password</a>
                        </div>
                        <div class="alert alert-danger bg-danger" style='display:none;' id="msg-pass">
                                <span></span>
                        </div>
                        <form method="post" id="form-data">
                            <input type="hidden" name="id_reset" id="id" value="<?= $this->session->userdata('id_user') ?>">
                            <input type="hidden" name="type" id="type" value="0">
                            <input class="form-control" type="password" name="r_newpass" id="password" placeholder="New Password" required onkeyup="checkPass();">
                            <input class="form-control" type="password" name="confirm" id="confirm" placeholder="Konfirmasi Password" required onkeyup="checkPass();">
                            <div class="form-button">
                                <button type="button" class="btn disbled" id="btnSimpan">Reset Password</button> <a href="<?= base_url('/') ?>">Kembali Login ?</a>
                            </div>
                        </form>
                        <div class="other-links">
                            <span><?= APP_COPYRIGHT ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
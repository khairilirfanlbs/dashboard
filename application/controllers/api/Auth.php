<?php
defined('BASEPATH') or exit('No direct script access allowed');

require(APPPATH . '/libraries/REST_Controller.php');

use Restserver\Libraries\REST_Controller;

class Auth extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();        
        // Load these helper to create JWT tokens
        $this->load->helper(['jwt', 'authorization']);
    }

    function index_get()
    {
        $payload_token = $this->verify_request();
        if ($payload_token != null) {
            var_dump($payload_token);die;
        }else{
            echo 'empty payload token';
        }

        // $this->response([
        //     'status' => isset($user) ? true : false,
        //     'data' => $user
        // ]);
    }


    function index_post()
    {
        // $payload_token = $this->verify_request();
        // var_dump($payload_token);die;

        $token_payload = [
            'time' => time(),
            'id' => 1,
            'username' => 'imran',
            'nama' => 'imran zulfri pulungan',
            'role_user' => 'customer'
        ];

        $token_generated =  AUTHORIZATION::generateToken($token_payload);
        var_dump($token_generated);die;
    }

    private function verify_request()
    {
        // Get all the headers
        $headers = $this->input->request_headers();

        // Extract the token
        $token = $headers['Authorization'];

        // Use try-catch
        // JWT library throws exception if the token is not valid
        try {
            // Validate the token
            // Successfull validation will return the decoded user data else returns false
            $data = AUTHORIZATION::validateToken($token);
            if ($data === false) {
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                $this->response($response, $status);

                exit();
            } else {
                return $data;
            }
        } catch (Exception $e) {
            // Token is invalid
            // Send the unathorized access message
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);
        }
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }
    
    public function proses(){
		$check = $this->m_login->proses();	
        $num = $check->num_rows();
        $getAkun = $check->row();
        if($num > 0){
            $this->session->set_userdata(array(
                            'status_login'  =>TRUE,
                            'id_user'       =>$getAkun->id,
							'nama'          =>$getAkun->nama,
                            'username'      =>$getAkun->username,
                            'email'         =>$getAkun->email,
                            'id_role'       =>$getAkun->id_role,
                        ));
                        $msg = array(
                            'msg'=>'Berhasil login',
                            'icon' => 'success',
                            'link' => 'dashboard',
                        );
        }else{
            $msg = array(
                'msg'=>'Akses Ditolak',
                'icon' => 'warning',
            );
        }
        echo json_encode($msg);
    }
    
}

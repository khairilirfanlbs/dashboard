<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//--- auth
//logout 
$route['logout']                                        = 'auth/logout';
//login
$route['masuk']                                         = 'auth/login';
$route['proses-login']                                  = 'auth/login/proses';
//forgot password
$route['forgot-password']                               = 'auth/forgotPassword';
$route['proses-forgot-password']                        = 'auth/forgotPassword/proses';
//reset password 
$route['reset-password']                                = 'auth/resetPassword';
$route['proses-reset-password']                         = 'auth/resetPassword/proses';
//register
$route['registrasi']                                    = 'auth/register';
$route['proses-register']                               = 'auth/register/proses';
//-------------------

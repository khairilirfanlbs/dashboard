<?php
if(!defined('BASEPATH')) exit('no file allowed');
function isAuth(){
    $Ci =& get_instance();
    $status_login = $Ci->session->userdata('status_login');
    if ($status_login == false) {
        redirect('/');
    }
}

function isLogged(){
    $Ci =& get_instance();
    $status_login = $Ci->session->userdata('status_login');
    if ($status_login == true) {
        redirect('dashboard');
    }
}

function getInfo(){
    $Ci =& get_instance();
    $id = $Ci->session->userdata('id_user');

    $getData    = $Ci->db->get_where('tbl_user',['id'=>$id])->row();
    $getPegawai = $Ci->db->select('*, tbl_user.id as iduser')
                         ->join('tbl_pegawai','tbl_pegawai.uuid = tbl_user.uuid')
                         ->join('tbl_user_role','tbl_user_role.id = tbl_user.id_role')
                         ->join('tbl_provinsi','tbl_provinsi.id = tbl_pegawai.id_provinsi')
                         ->get_where('tbl_user',['tbl_user.id'=>$id]);
    $getDokter  = $Ci->db->select('*, tbl_user.id as iduser')
                         ->join('tbl_dokter','tbl_dokter.uuid = tbl_user.uuid')
                         ->join('tbl_komoditi','tbl_komoditi.id = tbl_dokter.id_komoditi')
                         ->join('tbl_spesialis_dokter','tbl_spesialis_dokter.id = tbl_dokter.id_spesialis')
                         ->join('tbl_user_role','tbl_user_role.id = tbl_user.id_role')
                         ->get_where('tbl_user',['tbl_user.id'=>$id]);
    $getPetani  = $Ci->db->join('tbl_petani','tbl_petani.uuid = tbl_user.uuid')
                         ->get_where('tbl_user',['tbl_user.id'=>$id]);
    if($getPegawai->num_rows() == 0 && $getDokter->num_rows() == 0){
        return $getPetani->row();
    }elseif($getPegawai->num_rows() == 0 && $getPetani->num_rows() == 0){
        return $getDokter->row();
    }elseif($getDokter->num_rows() == 0 && $getPetani->num_rows() == 0){
        return $getPegawai->row();
    }
}

function getRole(){
    $Ci =& get_instance();
    $id = $Ci->session->userdata('id_user');

    $getData    = $Ci->db->get_where('tbl_user',['id'=>$id])->row();
    $getRole    = $Ci->db->get_where('tbl_user_akses',['id_role'=>$getData->id_role]);
    return $getRole->row();
}



